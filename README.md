# README #

This is a repo with T-Systems Java School preliminary examination tasks.
Code points where you solution is to be located are marked with TODOs.

The solution is to be written using Java 1.8 only, external libraries are forbidden. 
You can add dependencies with scope "test" if it's needed to write new unit-tests.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : Konstantin Polygalov
* Codeship : [ ![Codeship Status for tschool/javaschoolexam](https://app.codeship.com/projects/1a3476c0-1728-0136-39b3-3edfd05b1bcb/status?branch=master)](https://app.codeship.com/projects/283804)

Example of Codeship badge. Please remove the example before you send us the link. 
[ ![Codeship Status for tschool/javaschoolexam](https://app.codeship.com/projects/a9af8940-d130-0134-89a6-5e8aaaa2a5a2/status?branch=master)](https://app.codeship.com/projects/283804)
