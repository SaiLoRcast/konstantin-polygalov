package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Locale;

class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    String evaluate(String statement) {
        LinkedList<Double> numbers = new LinkedList<>();
        Deque<Character> operators = new ArrayDeque<>();
        Locale.setDefault(Locale.ENGLISH);
        DecimalFormat decimalFormat = new DecimalFormat("##.####");

        try {
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (c == ' ') continue;

                if (c == '(') {
                    operators.addLast('(');

                } else if (c == ')') {
                    while (operators.getLast() != '(') {
                        process(numbers, operators.removeLast());
                    }
                    operators.removeLast();

                } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                    while (!operators.isEmpty() && priority(operators.getLast()) >= priority(c)) {
                        process(numbers, operators.removeLast());
                    }
                    operators.add(c);

                } else {
                    String s = "";
                    while (i < statement.length() && Character.isDigit(statement.charAt(i)) | statement.charAt(i) == '.') {
                        s += statement.charAt(i++);
                    }
                    i--;
                    numbers.add(Double.parseDouble(s));
                }
            }

            while (!operators.isEmpty()) {
                process(numbers, operators.removeLast());
            }

            return numbers.get(0).isInfinite() || numbers.get(0).isNaN() ? null : decimalFormat.format(numbers.get(0));

        } catch (Exception e) {
            return null;
        }
    }

    private static int priority(char c) {
        switch (c) {
            case '+':
            case '-':
                return 0;
            case '*':
            case '/':
                return 1;
            default:
                return -1;
        }
    }

    private static void process(LinkedList<Double> list, char operation) {
        double a = list.removeLast();
        double b = list.removeLast();

        switch (operation) {
            case '+':
                list.add(b + a);
                break;
            case '-':
                list.add(b - a);
                break;
            case '*':
                list.add(b * a);
                break;
            case '/':
                list.add(b / a);
                break;
        }
    }
}
