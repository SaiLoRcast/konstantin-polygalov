package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers)throws CannotBuildPyramidException {
        try {
            Collections.sort(inputNumbers);
        } catch (Throwable e) {
            throw new CannotBuildPyramidException("It's not possible to build one");
        }

        int i = 1;
        int j = inputNumbers.size();
        while (j != 0) {
            j = j - i;
            i++;
            if (j < 0) {
                throw new CannotBuildPyramidException("It's not possible to build one");
            }
        }

        int length = --i * 2 - 1;
        int[][] arr = new int[i][length];
        int change = 0;
        int ii = i;
        int temp = i;

        for (int k = 0; k < temp; k++) {
            int arrIndex = 0;
            int index = 0;
            int value = 0;
            while (value < change) {
                arr[k][arrIndex] = 0;
                ++arrIndex;
                value++;
            }
            for (int l = change; l < length - change; l++) {
                if (change % 2 == 0) {
                    if (l % 2 == 0) {
                        arr[k][arrIndex] = inputNumbers.get(inputNumbers.size() - i + index);
                        index++;
                        ++arrIndex;
                    } else {
                        arr[k][arrIndex] = 0;
                        ++arrIndex;
                    }
                } else {
                    if (l % 2 == 1) {
                        arr[k][arrIndex] = inputNumbers.get(inputNumbers.size() - i + index);
                        index++;
                        ++arrIndex;
                    } else {
                        arr[k][arrIndex] = 0;
                        ++arrIndex;
                    }
                }
            }
            value = length - change;
            while (value < length) {
                value++;
                arr[k][arrIndex] = 0;
                ++arrIndex;
            }
            change++;
            ii--;
            i += ii;

        }
        int[][] massive = new int[temp][length];
        for (int f = 0; f < arr.length; f++) {
            for (int s = 0; s < arr[f].length; s++) {
                massive[arr.length - f - 1][s] = arr[f][s];
            }

        }
        return massive;
    }
}
